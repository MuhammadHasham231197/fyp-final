﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    public class ProgressValue
    {
        int overhead=0, lockthedoors=0, chinupdown=0, sides = 0;
        public ProgressValue(int overhead,int lockthedoors,int chinupdown,int sides)
        {
            this.overhead = overhead;
            this.lockthedoors = lockthedoors;
            this.chinupdown = chinupdown;
            this.sides = sides;
        }
        public int[] getValuesData()
        {
            int[] values = { this.overhead, this.lockthedoors, this.chinupdown, this.sides };
            Console.WriteLine(values[0]);
            return values;
        }
    }
}
