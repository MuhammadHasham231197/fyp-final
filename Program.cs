﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Controls;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Math.Optimization.Losses;
using Accord.Statistics;
using Accord.Statistics.Kernels;
using Accord.Controls;
using Accord.IO;
using Accord.Math;
using Accord.Statistics.Distributions.Univariate;
using Accord.MachineLearning.Bayes;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
namespace Microsoft.Samples.Kinect.SkeletonBasics
{

    class Program
    {


        [MTAThread]
        public static bool func(double[][] inputs)
        {
            //double[][] problem = new double[100][];
            /*double[][] problem =
            {
                //             a    b    a + b
               new double[] { 0,   0,     0,0   ,0,0},
               new double[] { 0,   0,     0,0   ,0,0 },
               new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
                new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
               new double[] { 0,   0,     0,0    ,0,0},
            };*/
            //for (int n = 0; n < problem.Length; n++)
            //{

            // Print the row number 

            //  problem[n] = new double[] { 0, 0, 0, 0, 0, 0 };

            //}
            /*Excel.Application xlApp = new Excel.Application();
             Excel.Workbook xlWorkbook = xlApp.Workbooks.Open("F:\\FYP\\rehabilitation-exercise\\Dataset\\dataset.xlsx");
             Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
             Excel.Range xlRange = xlWorksheet.UsedRange;

            for (int i = 1; i < problem.Length; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    //new line
                    if (j == 1)
                        Console.Write("\r\n");

                    //write the value to the console
                    if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j].Value2 != null && i!=1)
                        Console.Write("\t");
                    Double result = 0;
                    if (Double.TryParse(xlRange.Cells[i, j].Value2.ToString(), out result) && i != 1)
                    {
                        problem[i][j] = result;
                        Console.Write(problem[i][j]);
                    }
                }
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            



            */

            // Get the two first columns as the problem
            // inputs and the last column as the output

            // input columns

            // double[][] inputs = problem.GetColumns(0, 4);

            // output column
            // int[] outputs = problem.GetColumn(5).ToInt32();

            // Create the learning algorithm with the chosen kernel
            /*var teacher = new LinearDualCoordinateDescent()
            {
                Loss = Loss.L2,
                Complexity = 1000,
                Tolerance = 1e-5
            };*/


            // Use the algorithm to learn the machine
            /*BinaryFormatter formatterOutput = new BinaryFormatter();
            FileStream fsOutput = new FileStream("svm.bin", FileMode.Open);
            var bayes = (Accord.MachineLearning.VectorMachines.SupportVectorMachine)formatterOutput.Deserialize(fsOutput);
            Console.WriteLine("Press any key to exit.");*/


            //var svm = teacher.Learn(inputs, outputs);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream("svm2.bin", FileMode.Open);
            //formatter.Serialize(fs, svm);
            //'Unable to cast object of type 'Accord.MachineLearning.VectorMachines.SupportVectorMachine`1
            //[Accord.Statistics.Kernels.Polynomial]' 
            //to type 'Accord.MachineLearning.VectorMachines.SupportVectorMachine
            var bayes = (Accord.MachineLearning.VectorMachines.SupportVectorMachine)formatter.Deserialize(fs);
            // Compute the machine's answers for the learned inputs
            double[][] psroblem =
            {
                //             a    b    a + b
               new double[] {179 ,   169, 26 , 102, 128 ,138 ,108 ,19 , 24 , 8  , 6},


            };

            Console.WriteLine("Evaluating SVM model");
            /* bool[] preds = bayes.Decide(problem);
             double[] score = bayes.Score(problem);
             int[] y = { 1 };

             int numCorrect = 0; int numWrong = 0;
             for (int i = 0; i < preds.Length; ++i)
             {
                 Console.Write("Predicted (double) : " + score[i] + " ");
                 Console.Write("Predicted (bool): " + preds[i] + " ");
                 Console.WriteLine("Actual: " + y[i]);
                 if (preds[i] == true && y[i] == 1) ++numCorrect;
                 else if (preds[i] == false && y[i] == -1) ++numCorrect;
                 else ++numWrong;
             }
             double acc = (numCorrect * 100.0) / (numCorrect + numWrong);
             Console.WriteLine("Model accuracy = " + acc);*/





            double[][] input = inputs.GetColumns(0, 10);
            //Console.WriteLine(inputs);
            bool[] answers = bayes.Decide(input);
            Console.WriteLine(psroblem[0]);
            for (int i = 0; i < answers.Length; i++)
            {
                Console.WriteLine(answers[i]);
            }
            // Convert to Int32 so we can plot:
            //int[] zeroOneAnswers = answers.ToZeroOne();

            // Plot the results
            //ScatterplotBox.Show("SVM's answer", inputs, zeroOneAnswers)
            //  .Hold();
            fs.Close();
            return answers[0];

        }
    }
}
