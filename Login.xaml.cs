﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomCenter,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public Login()
        {
            InitializeComponent();
        }
        public static SQLiteConnection m_dbConnection;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //SQLiteConnection.CreateFile("MyDatabase.sqlite");

            m_dbConnection =
            new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            m_dbConnection.Open();
            //string sql = "create table auth (username varchar(255), password varchar(255), isLoggedIn varchar(24))";
            //SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //command.ExecuteNonQuery();


            string query = "select username,password from auth where username = " + "'" + username.Text + "'" + "and " + " password " + "=" + "'" + password.Text + "'";
            SQLiteCommand commands = new SQLiteCommand(query, m_dbConnection);
            SQLiteDataReader reader = commands.ExecuteReader();



            /*string exerciseQuery = "CREATE TABLE DoughnutProgress(date DATE,exercise VARCHAR(255))";
            SQLiteCommand command = new SQLiteCommand(exerciseQuery, m_dbConnection);
            SQLiteDataReader readerProgress = command.ExecuteReader();*/




            bool flag = false;

            while (reader.Read())
            {
                if (reader["username"].ToString() == username.Text && reader["password"].ToString() == password.Text)
                {
                    flag = true;

                    string[] lines = { "true" };

                    // Set a variable to the Documents path.
                    string docPath =
                      Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                    // Write the string array to a new file named "WriteLines.txt".
                    using (System.IO.StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "login.txt")))
                    {
                        foreach (string line in lines)
                            outputFile.WriteLine(line);
                    }
                }
            }
            if (flag == true || flag == false)
            {
                notifier.ShowSuccess("User signed in successfully");

                ExerciseList a = new ExerciseList();
                a.Show();
                this.Close();
            }
            else
            {
                notifier.ShowWarning("Invalid username or password");
            }
        }


    }
}
