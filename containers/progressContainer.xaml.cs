﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using System.Linq;
using System.Windows;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.Data.SQLite;

namespace Microsoft.Samples.Kinect.SkeletonBasics.Exercises
{
    /// <summary>
    /// Interaction logic for progressContainer.xaml
    /// </summary>
    public partial class progressContainer : UserControl
    {
        public progressContainer()
        {
            InitializeComponent();

            
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=D:\\db.sqlite;Version=3;");
            m_dbConnection.Open();
            int overhead=0, lockthedoors=0, chinupdown=0, sides = 0;
            DateTime dt = DateTime.Now;
            string sql = "SELECT exercise,count(*) as count FROM exerciseprogress where month=" + dt.Month + " group by exercise ;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            Console.WriteLine(dt.Month.ToString());
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if(string.Compare(reader["exercise"].ToString(), "Overhead") != -1)
                {
                    overhead = Convert.ToInt32(reader["count"]);
                }
                else if(string.Compare(reader["exercise"].ToString(), "LockTheDoors") != -1)
                {
                    lockthedoors = Convert.ToInt32(reader["count"]);
                }
                else if(string.Compare(reader["exercise"].ToString(), "ChinUpDown") != -1)
                {
                    chinupdown = Convert.ToInt32(reader["count"]);
                }
                else if(string.Compare(reader["exercise"].ToString(), "Sides") != -1)
                {
                    sides = Convert.ToInt32(reader["count"]);
                }
                Console.WriteLine("Name: " +reader["exercise"] + "\tScore: " + reader["count"]);
            }
            ProgressValue value = new ProgressValue(overhead, lockthedoors, chinupdown, sides);
            int[] values = value.getValuesData();
            
            SeriesCollection = new SeriesCollection
            {
                new PieSeries
                {
                    Title = "ChinUpDown",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(values[0]) },
                    DataLabels = true
                },
                new PieSeries
                {
                    Title = "LockTheDoors",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(values[1]) },
                    DataLabels = true
                },
                new PieSeries
                {
                    Title = "Overhead",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(values[2]) },
                    DataLabels = true
                },
                new PieSeries
                {
                    Title = "Sides",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(values[3]) },
                    DataLabels = true
                }
            };

            //adding values or series will update and animate the chart automatically
            //SeriesCollection.Add(new PieSeries());
            //SeriesCollection[0].Values.Add(5);

            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }

        private void UpdateAllOnClick(object sender, RoutedEventArgs e)
        {
            var r = new Random();

            foreach (var series in SeriesCollection)
            {
                foreach (var observable in series.Values.Cast<ObservableValue>())
                {
                    observable.Value = r.Next(0, 10);
                }
            }
        }

        private void AddSeriesOnClick(object sender, RoutedEventArgs e)
        {
            var r = new Random();
            var c = SeriesCollection.Count > 0 ? SeriesCollection[0].Values.Count : 5;

            var vals = new ChartValues<ObservableValue>();

            for (var i = 0; i < c; i++)
            {
                vals.Add(new ObservableValue(r.Next(0, 10)));
            }

            SeriesCollection.Add(new PieSeries
            {
                Values = vals
            });
        }

        private void RemoveSeriesOnClick(object sender, RoutedEventArgs e)
        {
            if (SeriesCollection.Count > 0)
                SeriesCollection.RemoveAt(0);
        }

        private void AddValueOnClick(object sender, RoutedEventArgs e)
        {
            var r = new Random();
            foreach (var series in SeriesCollection)
            {
                series.Values.Add(new ObservableValue(r.Next(0, 10)));
            }
        }

        private void RemoveValueOnClick(object sender, RoutedEventArgs e)
        {
            foreach (var series in SeriesCollection)
            {
                if (series.Values.Count > 0)
                    series.Values.RemoveAt(0);
            }
        }

        private void RestartOnClick(object sender, RoutedEventArgs e)
        {
            Chart.Update(true, true);
        }
    }

}
