﻿using BespokeFusion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for shoulderContainer.xaml
    /// </summary>
    public partial class shoulderContainer : UserControl
    {
        bool isWindowOpen = false;
        public shoulderContainer()
        {
            InitializeComponent();
            
            exerciseOne.Click += callOverHead;
            exerciseTwo.Click += exerciseTwo_Click_1;
            
        }
        

        private void callOverHead(object sender, RoutedEventArgs e)
        {

           // MessageBox.Show("Hello");
            
           MainWindow newwindow = new MainWindow("OverHead_Shoulder");
           newwindow.Show();
            
            
            //this.Content = main;
           // main.Content = new Frame();
          //  Frame f = new Frame();
            //this.NavigationService.Navigate(new MainWindow("OverHead_Shoulder"));
        }

        private void exerciseTwo_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (Window w in Application.Current.Windows)
            {
                if (w is MainWindow)
                {
                    isWindowOpen = true;
                    w.Activate();
                }
            }

            if (!isWindowOpen)
            {
                MainWindow newwindow = new MainWindow("LockTheDoors_Shoulder");
                newwindow.Show();
            }

            //Frame f = new Frame();
            //this.NavigationService.Navigate(new MainWindow("LockTheDoors_Shoulder", currMode));
        }

        private void info1_Click(object sender, RoutedEventArgs e)
        {
            MaterialMessageBox.Show(
                "• Perform this exercise while sitting." +
                "\n• Begin by positioning your arms in a way that both your arms are in 'L' shape " +
                "\n• Once you have it covered then slowly move your hands over your head while trying not to bend the hands." +
                "\n• Then slowly return back to the first posture." +
                "\n• Repeat the steps until the specified count."
                    , "Steps - Overhead (Shoulder)");
        }

        private void info2_Click(object sender, RoutedEventArgs e)
        {
            MaterialMessageBox.Show(
                "• Perform this exercise while sitting." +
                "\n• Begin by stretching your arms straight in front of your body " +
                "\n• Keep this posture for 5 seconds.." +
                "\n• Simultaneously, rotate your claw in clockwise and anti-clockwise direction repeatedly."
                    , "Steps - Overhead (Shoulder)");
        }
    }
}
