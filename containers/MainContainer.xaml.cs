﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for MainContainer.xaml
    /// </summary>
    public partial class MainContainer : UserControl
    {
        public MainContainer()
        {
            InitializeComponent();
   
        }

        private void NeckEx_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void NeckEx_Click_1(object sender, RoutedEventArgs e)
        {
            MainContainer list = new MainContainer();


           // main.Children.Add(new neckContainer());
            main.Children.Clear();
            main.Children.Add(new neckContainer());
        }

        

        private void ShoulderEx_Click_1(object sender, RoutedEventArgs e)
        {
            main.Children.Clear();
            main.Children.Add(new shoulderContainer());
        }
    }
}
