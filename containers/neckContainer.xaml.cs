﻿using BespokeFusion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for neckContainer.xaml
    /// </summary>
    public partial class neckContainer : UserControl
    {
        public neckContainer()
        {
            InitializeComponent();
            headupHeaddown.Click += headupHeaddown_Click;
            earToShoulder.Click += earToShoulder_Click;
        }


        private void headupHeaddown_Click(object sender, RoutedEventArgs e)
        {

            MainWindow newwindow = new MainWindow("headUpHeadDown_Neck");
            newwindow.Show();
            //this.NavigationService.Navigate(new MainWindow("headUpHeadDown_Neck", currMode));
        }


        private void earToShoulder_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newwindow = new MainWindow("earToShoulder_Neck");
            newwindow.Show();
            //this.NavigationService.Navigate(new MainWindow("earToShoulder_Neck", currMode));
        }

        private void info1_Click(object sender, RoutedEventArgs e)
        {


            MaterialMessageBox.Show(
                "• Perform this exercise while sitting. " +
                "\n• Begin by slowly tilting your head down so your chin meets the chest. " +
                "\n• Maintain the position for around five seconds, and gradually return back to the normal position." +
                "\n• Then tilt your head upwards and keep your eyes on the ceiling for five more seconds." +
                "\n• Return back again to the normal position."
                    , "Steps - Neck down/ Neck up");
        }

        private void info2_Click(object sender, RoutedEventArgs e)
        {
            MaterialMessageBox.Show(
                "• Perform this exercise while sitting. " +
                "\n• Begin by slowly tilting your head to your left so that your ear meets your left shoulder." +
                "\n• Maintain the position for around five seconds, and gradually return back to the normal position." +
                "\n• Similarly, tilt your head towards right shoulder for five more seconds." +
                "\n• Return back again to the normal position."
                    , "Steps - Ear to shoulder");
        }
    }
}
