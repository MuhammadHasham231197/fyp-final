﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Samples.Kinect.SkeletonBasics

{
    /// <summary>
    /// Interaction logic for Frame.xaml
    /// </summary>
    public partial class Frame : Window
    {
        public Frame()
        {
            InitializeComponent();

            Loaded += Frame_Loaded;
        }

        private void Frame_Loaded(object sender, RoutedEventArgs e)
        {
           
            frame.NavigationService.Navigate(new ExerciseList());
        }
    }
}
