﻿using BespokeFusion;
using Microsoft.Samples.Kinect.SkeletonBasics.Exercises;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications.Messages;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for ExerciseList.xaml
    /// </summary>
    public partial class ExerciseList : Window
    {
       
        Settings settingScreen;
        String mode=Settings.CMODE;
        currentMode cm = new currentMode();


        public static SQLiteConnection m_dbConnection;

        public ExerciseList()
        { 

            InitializeComponent();
            
            //SQLiteConnection.CreateFile("D:\\db.sqlite");

            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=D:\\db.sqlite;Version=3;");
            m_dbConnection.Open();

           

            /*Delete Table*/

            //string query1 = "DELETE FROM " + "exerciseprogress";
            //SQLiteCommand command2 = new SQLiteCommand(query1, m_dbConnection);
            //command2 = new SQLiteCommand(query1, ExerciseList.m_dbConnection);
            //command2.ExecuteNonQuery();

            /*Table creation*/
            //string query = "create table exerciseprogress (month varchar(255),exercise varchar(255))";
            //SQLiteCommand command = new SQLiteCommand(query, m_dbConnection);
            //command.ExecuteNonQuery();

            /*Insertion in table*/
            //DateTime dt = DateTime.Now;
            //string sql1 = "insert into exerciseprogress (month,exercise) values (" +"'"+  dt.Month +"'," + "'ChinUpDown')";
            //SQLiteCommand command0 = new SQLiteCommand(sql1, m_dbConnection);
            //command0.ExecuteNonQuery();
            //m_dbConnection.Close();

            /*bool flag = true;
            try
            {   // Open the text file using a stream reader.
                string docPath =
                     Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                using (StreamReader sr = new StreamReader(docPath+"/"+"TestFile.txt"))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    Console.WriteLine(line);
                    if(line == "true")
                    {
                        flag = false;
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            if(flag == true)
            {
              
                Login n = new Login();
                n.Show();
            }*/


            //ShoulderEx.Click += exerciseOne_Click;
            //settingScreen = new Settings();
            //NeckEx.Click += NeckEx_Click;

            SidePanelHome.Visibility = Visibility.Visible;
            SidePanelProgress.Visibility = Visibility.Hidden;
            SidePanelManual.Visibility = Visibility.Hidden;

            homeBtn.Background.Opacity = 0.5;

            root.Children.Clear();
            root.Children.Add(new MainContainer());
        }

        private void exerciseOne_Click(object sender, RoutedEventArgs e) // shoulder
        {
            
            Frame f = new Frame();
            //this.NavigationService.Navigate(new StartScreen(Settings.CMODE));
            
        }

        private void NeckEx_Click(object sender, RoutedEventArgs e) // neck
        {
            Frame f = new Frame();
            //this.NavigationService.Navigate(new NeckExercises(Settings.CMODE));
        }

        private void setting_Click(object sender, RoutedEventArgs e)
        {
            settingScreen.Show();
        }
            
        private void Button_Click(object sender, RoutedEventArgs e) //home button
        {
            SidePanelHome.Visibility = Visibility.Visible;
            SidePanelProgress.Visibility = Visibility.Hidden;
            SidePanelManual.Visibility = Visibility.Hidden;

            progressBtn.Background.Opacity = 0;
            homeBtn.Background.Opacity = 0.5;
            manualBtn.Background.Opacity = 0;

            root.Children.Clear();
            root.Children.Add(new MainContainer());

        }

        private void Button_Click_1(object sender, RoutedEventArgs e) //progress button
        {
            SidePanelHome.Visibility = Visibility.Hidden;
            SidePanelProgress.Visibility = Visibility.Visible;
            SidePanelManual.Visibility = Visibility.Hidden;

            homeBtn.Background.Opacity = 0;
            progressBtn.Background.Opacity = 0.5;
            manualBtn.Background.Opacity = 0;

            root.Children.Clear();
            root.Children.Add(new progressContainer());

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SidePanelHome.Visibility = Visibility.Hidden;
            SidePanelProgress.Visibility = Visibility.Hidden;
            SidePanelManual.Visibility = Visibility.Visible;
            //Program.func();
            homeBtn.Background.Opacity = 0;
            manualBtn.Background.Opacity = 0.5;
            progressBtn.Background.Opacity = 0;

  
            root.Children.Clear();
            root.Children.Add(new manualContainer());


        }


     

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var msg = new CustomMaterialMessageBox
            {
                TxtMessage = { Text = "Are you sure you want to exit?", Foreground = Brushes.Black },
                TxtTitle = { Text = "Confirmation", Foreground = Brushes.White },
                BtnOk = { Content = "Yes", Background = Brushes.Red },
                BtnCancel = { Content = "No" },
                BtnCopyMessage = { Visibility = Visibility.Hidden },
                TitleBackgroundPanel = { Background = Brushes.Red },

                BorderBrush = Brushes.Red
            };

            msg.Show();
            var results = msg.Result;

            if (results + "" == "OK")
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            string query = "DELETE FROM " + "auth";
            SQLiteCommand command = new SQLiteCommand(query, ExerciseList.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            command = new SQLiteCommand(query, ExerciseList.m_dbConnection);
            command.ExecuteNonQuery();

            Login n = new Login();
            n.Show();

           
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            //Program.func();
        }
    }
}
