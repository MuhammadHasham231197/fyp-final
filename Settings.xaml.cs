﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using ToastNotifications.Messages;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomCenter,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public static string  CMODE;
        currentMode cm = new currentMode();
        
        public Settings()
        {

            InitializeComponent();

            this.Closing += new System.ComponentModel.CancelEventHandler(Window_Closing);

            /*if (cm.getMode().Equals("test"))
            {
                MessageBox.Show(cm.getMode());
                test_mode.IsChecked = true;
            }
            else if (cm.getMode().Equals("custom"))
            {
                custom_mode.IsChecked = true;
            }
            else
                default_mode.IsChecked = true;*/

            

        }
        void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            default_mode.IsChecked = true;
            cm.setMode("default");
            CMODE = "default";

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;

            notifier.ShowInformation("Default mode selected.");


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            notifier.ShowSuccess("Changes saved successfuly.");

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            test_mode.IsChecked = true;
            cm.setMode("test");
            CMODE = "test";
            
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {        
            custom_mode.IsChecked = true;
            cm.setMode("custom");
            CMODE = "custom";
        }

        private void default_mode_Checked(object sender, RoutedEventArgs e)
        {          
            default_mode.IsChecked = true;
            cm.setMode("default");
            CMODE = "default";
        }
    }
}
