﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System.Windows.Controls;
    using System;
    using System.Media;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Media.Imaging;
    using BespokeFusion;
    using ToastNotifications;
    using ToastNotifications.Position;
    using ToastNotifications.Lifetime;
    using ToastNotifications.Messages;
    using WpfAnimatedGif;
    using System.Data.SQLite;
    using System.Data;

    // Interaction logic for MainWindow.xaml
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Intermediate storage for the color data received from the camera
        /// </summary>
        private byte[] colorPixels;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        /// 
        SoundPlayer correct = new SoundPlayer("F:/FYP/correct.wav");
        SoundPlayer TaDa = new SoundPlayer("F:/FYP/TaDa.wav");
        SoundPlayer tick = new SoundPlayer("F:/FYP/tick.wav");

        //choose the first sensor amongst a number of sensors
        #region "Variables"
        // Thickness of body center ellipse
        private const double BodyCenterThickness = 10;
        // Thickness of clip edge rectangles
        private const double ClipBoundsThickness = 10;
        // Brush used to draw skeleton center point
        private readonly Brush centerPointBrush = Brushes.Blue;
        // Brush used for drawing joints that are currently tracked
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));
        // Brush used for drawing joints that are currently inferred
        private readonly Brush inferredJointBrush = Brushes.Yellow;
        // Pen used for drawing bones that are currently tracked
        private Pen trackedBonePen = new Pen(Brushes.Red, 6);
        // Pen used for drawing bones that are currently inferred
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);
        // Drawing image that we will display
        private DrawingImage imageSource;
        // Thickness of drawn joint lines
        private const double JointThickness = 3;
        // Drawing group for skeleton rendering output
        private DrawingGroup drawingGroup;
        // Width of output drawing
        private const float RenderWidth = 640.0f;
        // Height of our output drawing
        private const float RenderHeight = 480.0f;
        #endregion
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer2 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer3 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer FzShoulder1 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer FzShoulder2 = new System.Windows.Threading.DispatcherTimer();

       

        public string currMode = "default";
        private String send;
        private String sendMode;

        Boolean fzShoulder1 = false;
        public static Skeleton skeleton = new Skeleton();
        string currentExercise = "";
        Boolean prevSecFZ2 = false;
        Boolean fzShoulder2 = false;
        Boolean prevSecFZ3 = false;
        Settings settingScreen;
        SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=D:\\db.sqlite;Version=3;");
       

        public MainWindow()
        {
            InitializeComponent();

            
            m_dbConnection.Open();
            //After Initialization subscribe to the loaded event of the form 
            Loaded += MainWindow_Loaded;
            settingScreen = new Settings();

            //After Initialization subscribe to the unloaded event of the form
            //We use this event to stop the sensor when the application is being closed.
            Unloaded += MainWindow_Unloaded;

        }


        currentMode cm = new currentMode();

        public string exerciseType = "";
        public MainWindow(String sender)
        {
            InitializeComponent();


            //After Initialization subscribe to the loaded event of the form 
            Loaded += MainWindow_Loaded;
            settingScreen = new Settings();
            //After Initialization subscribe to the unloaded event of the form
            //We use this event to stop the sensor when the application is being closed.
            Unloaded += MainWindow_Unloaded;



            report.Visibility = Visibility.Collapsed;

            this.send = sender;
            this.sendMode = sender;

            Execise1.Content = this.send;
            SetNo.Content = "";
            setCount.Content = "";
            RepNo.Content = "";
            count.Content = "";


            if (sender.Equals("LockTheDoors_Shoulder"))
            {

                getReadyIn.Content = "Time Left: ";
                exerciseType = "Timer";
                TypeOFExercise(exerciseType);



            }
            else if (sender.Equals("OverHead_Shoulder"))
            {


                {
                    exerciseType = "Sets";
                    TypeOFExercise(exerciseType);
                }
            }
            else if (sender.Equals("headUpHeadDown_Neck"))
            {

                {
                    getReadyIn.Content = "Time Left: ";
                    exerciseType = "Timer";
                    TypeOFExercise(exerciseType);
                }
            }
            else if (sender.Equals("earToShoulder_Neck"))
            {


                {
                    getReadyIn.Content = "Time Left: ";
                    exerciseType = "Timer";
                    TypeOFExercise(exerciseType);
                }
            }

        }
        void TypeOFExercise(String type)
        {
            if (type == "Timer")
            {
                // when sets are not needed
                SetNo.Content = "";
                RepNo.Content = "";
                timer.Content = seconds;
            }
            else if (type == "Sets")
            {
                SetNo.Content = "Set No.";
                RepNo.Content = "Reps";
            }
        }
        void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            //stop the Sestor 
            sensor.Stop();

        }


        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

            //Create a Drawing Group that will be used for Drawing 
            this.drawingGroup = new DrawingGroup();

            //Create an image Source that will display our skeleton
            this.imageSource = new DrawingImage(this.drawingGroup);

            //Display the Image in our Image control
            
            Image.Source = imageSource;
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the color stream to receive color frames
                this.sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

                // Allocate space to put the pixels we'll receive
                this.colorPixels = new byte[this.sensor.ColorStream.FramePixelDataLength];

                // This is the bitmap we'll display on-screen
                this.colorBitmap = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

                // Set the image we display to point to the bitmap where we'll put the image data
                this.Image1.Source = this.colorBitmap;

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.ColorFrameReady += this.SensorColorFrameReady;

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }


            try
            {
                //Check if the Sensor is Connected
                if (sensor.Status == KinectStatus.Connected)
                {
                    //Start the Sensor
                    sensor.Start();
                    //Tell Kinect Sensor to use the Default Mode(Human Skeleton Standing) || Seated(Human Skeleton Sitting Down)
                    sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    //Subscribe to te  Sensor's SkeletonFrameready event to track the joins and create the joins to display on our image control
                    sensor.SkeletonFrameReady += sensor_SkeletonFrameReady;
                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = "  Kinect Ready";
                    Message.Background = new SolidColorBrush(Colors.Green);
                    Message.Foreground = new SolidColorBrush(Colors.White);

                    
                    // Turn on the skeleton stream to receive skeleton frames
                    this.sensor.SkeletonStream.Enable();
                    //load.Visibility = Visibility.Hidden;
                }
                else if (sensor.Status == KinectStatus.Disconnected)
                {
                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = " Kinect Sensor is not Connected";
                    Message.Background = new SolidColorBrush(Colors.Orange);
                    Message.Foreground = new SolidColorBrush(Colors.Black);

                }
                else if (sensor.Status == KinectStatus.NotPowered)
                {
                    //nice message with Colors to alert you if your sensor is working or not
                    Message.Text = " Kinect Sensor is not Powered";
                    Message.Background = new SolidColorBrush(Colors.Red);
                    Message.Foreground = new SolidColorBrush(Colors.Black);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        /*This function is used to get the angle between 3 joints input to the function
        Vector algebra in 3D is used for the purpose.
        The variable joint2 is used a fulcrum.
        The absolute x, y and z coordinates of the joints obtainted.
        their relative distance is calculated.
        using dot product concept
        angle cos(x) = (vect(A).vect(B)) / (mag(vect(A)) x mag(vect(B)))
         */
        double GetAngle(Joint joint1, Joint joint2, Joint joint3)
        {
            double x1 = joint1.Position.X;
            // x coordinate of joint1 
            double y1 = joint1.Position.Y;
            // y coordinate of joint1 
            double z1 = joint1.Position.Z;
            // z coordinate of joint1 

            double x2 = joint2.Position.X;
            // x coordinate of joint2 
            double y2 = joint2.Position.Y;
            // y coordinate of joint2 
            double z2 = joint2.Position.Z;
            // z coordinate of joint2 

            double x3 = joint3.Position.X;
            // x coordinate of joint3 
            double y3 = joint3.Position.Y;
            // y coordinate of joint3 
            double z3 = joint3.Position.Z;
            // z coordinate of joint3 

            double shiftx1 = x1 - x2;
            // x coordinate - Relative position of joint1 with respect to joint2
            double shifty1 = y1 - y2;
            // y coordinate - Relative position of joint1 with respect to joint2
            double shiftz1 = z1 - z2;
            // z coordinate - Relative position of joint1 with respect to joint2

            double shiftx2 = x3 - x2;
            // x coordinate - Relative position of joint3 with respect to joint2
            double shifty2 = y3 - y2;
            // y coordinate - Relative position of joint3 with respect to joint2
            double shiftz2 = z3 - z2;
            // z coordinate - Relative position of joint3 with respect to joint2

            double product = shiftx1 * shiftx2 + shifty1 * shifty2 + shiftz1 * shiftz2;
            // dot product
            double mag1 = Math.Abs(Math.Sqrt(Math.Pow(shiftx1, 2) + Math.Pow(shifty1, 2) + Math.Pow(shiftz1, 2)));
            // magnitude of vector 1
            double mag2 = Math.Abs(Math.Sqrt(Math.Pow(shiftx2, 2) + Math.Pow(shifty2, 2) + Math.Pow(shiftz2, 2)));
            // magnitude of Vector 2
            double temp = product / (mag1 * mag2);
            double angle = (180 / Math.PI) * Math.Acos(temp);
            return angle;
        }

        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    // Copy the pixel data from the image to a temporary array
                    colorFrame.CopyPixelDataTo(this.colorPixels);

                    // Write the pixel data into our bitmap
                    this.colorBitmap.WritePixels(
                        new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                        this.colorPixels,
                        this.colorBitmap.PixelWidth * sizeof(int),
                        0);
                }
            }
        }


        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.Stop();
            }
        }

        double GetDistance(Joint joint1, Joint joint2)
        {
            load.Visibility = Visibility.Hidden;
            positionText.Visibility = Visibility.Hidden;
            double x1 = joint1.Position.X;
            // x coordinate of joint1 
            double y1 = joint1.Position.Y;
            // y coordinate of joint1 
            double z1 = joint1.Position.Z;
            // z coordinate of joint1 

            double x2 = joint2.Position.X;
            // x coordinate of joint2 
            double y2 = joint2.Position.Y;
            // y coordinate of joint2 
            double z2 = joint2.Position.Z;
            // z coordinate of joint2 


            double shiftx1 = x1 - x2;
            // x coordinate - Relative position of joint1 with respect to joint2
            double shifty1 = y1 - y2;
            // y coordinate - Relative position of joint1 with respect to joint2
            double shiftz1 = z1 - z2;
            // z coordinate - Relative position of joint1 with respect to joint2


            double mag1 = Math.Abs(Math.Sqrt(Math.Pow(shiftx1, 2) + Math.Pow(shifty1, 2))) * 100;
            // magnitude of vector 1


            return mag1;
        }

        //get the skeleton closest to the Kinect sensor
        private static Skeleton GetPrimarySkeleton(Skeleton[] skeletons)
        {
            Skeleton skeleton = null;
            if (skeletons != null)
            {
                // if skeletons exits in the frame
                for (int i = 0; i < skeletons.Length; i++)
                {
                    if (skeletons[i].TrackingState == SkeletonTrackingState.Tracked)
                    {
                        if (skeleton == null)
                        {
                            skeleton = skeletons[i];
                        }
                        else
                        {
                            if (skeleton.Position.Z > skeletons[i].Position.Z)
                            {
                                skeleton = skeletons[i];
                            }
                        }
                    }
                }
            }
            return skeleton;
        }

      
        double angleLeft; // angle b/w left wrist, elbow, shoulder
        double angleRight; // angle b/w right wrist, elbow, shoulder
        double distanceRight; // dist. b/w right wrist to spine
        double distanceLeft; // dist. b/w left wirst to spine
        double wristDistance;
        double neckCenter; // angle b/w neck, centerShoulder, spine
        double neckLeft;
        double neckRight;
        double rightElbowShoulderShoulderCenter;
        double leftElbowShoulderShoulderCenter;
        double shoulderCenterShoulderLeftHead;
        double shoulderCenterShoulderRightHead;
        double rightElbowRightShoulderShoulderCenter;
        double leftShoulderElbowShoulderCenter;
        double rightShoulderElbowShoulderCenter;

        //double arr[][] = {angleLeft

        
        void sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            //declare an array of Skeletons
            Skeleton[] skeletons = new Skeleton[6];

            //Opens a SkeletonFrame object, which contains one frame of skeleton data.
            using (SkeletonFrame skeletonframe = e.OpenSkeletonFrame())
            {
                //Check if the Frame is Indeed open 
                if (skeletonframe != null)
                {

                    skeletons = new Skeleton[skeletonframe.SkeletonArrayLength];

                    // Copies skeleton data to an array of Skeletons, where each Skeleton contains a collection of the joints.
                    skeletonframe.CopySkeletonDataTo(skeletons);

                    Skeleton skeleton = GetPrimarySkeleton(skeletons);
                    if (skeleton != null)
                    {
                        // Get 3 joints - rightelbow, rightwrist and rightshoulder
                        // You can take any 3 joints of your choice from the 20 available.


                        Joint leftelbow = skeleton.Joints[JointType.ElbowLeft];
                        Joint leftwrist = skeleton.Joints[JointType.WristLeft];
                        Joint leftshoulder = skeleton.Joints[JointType.ShoulderLeft];

                        Joint rightelbow = skeleton.Joints[JointType.ElbowRight];
                        Joint rightwrist = skeleton.Joints[JointType.WristRight];
                        Joint rightshoulder = skeleton.Joints[JointType.ShoulderRight];

                        Joint neck = skeleton.Joints[JointType.Head];
                        Joint centerShoulder = skeleton.Joints[JointType.ShoulderCenter];
                        Joint spine = skeleton.Joints[JointType.Spine];
                        Joint shoulderCenter = skeleton.Joints[JointType.ShoulderCenter];
                        // Get the angle between the joints.

                        angleLeft = GetAngle(leftwrist, leftelbow, leftshoulder);
                        angleRight = GetAngle(rightwrist, rightelbow, rightshoulder);
                        distanceRight = GetDistance(rightwrist, spine);
                        distanceLeft = GetDistance(leftwrist, spine);
                        wristDistance = GetDistance(rightwrist, leftwrist);

                        rightElbowShoulderShoulderCenter = GetAngle(rightelbow, rightshoulder, shoulderCenter);
                        leftElbowShoulderShoulderCenter = GetAngle(leftelbow, leftshoulder, shoulderCenter);
                        shoulderCenterShoulderLeftHead = GetAngle(shoulderCenter, leftshoulder, neck);
                        shoulderCenterShoulderRightHead = GetAngle(shoulderCenter, rightshoulder, neck);
                        rightElbowRightShoulderShoulderCenter = GetAngle(rightelbow, rightshoulder, shoulderCenter);
                        leftShoulderElbowShoulderCenter = GetAngle(leftshoulder, leftelbow, shoulderCenter);
                        rightShoulderElbowShoulderCenter = GetAngle(rightshoulder, rightelbow, shoulderCenter);

                        neckCenter = GetAngle(neck, centerShoulder, spine);
                        neckLeft = GetAngle(neck, centerShoulder, leftshoulder);
                        neckRight = GetAngle(neck, centerShoulder, rightshoulder);


                        //MessageBox.Show(send);
                        if (send.Equals("OverHead_Shoulder"))
                        {
                            gif1.Visibility = Visibility.Visible;
                            gif2.Visibility = Visibility.Hidden;
                            gif3.Visibility = Visibility.Hidden;
                            gif4.Visibility = Visibility.Hidden;
                            loading.Visibility = Visibility.Hidden;
                            sheeda.Visibility = Visibility.Hidden;
                            ShoulderExercise1(angleLeft, angleRight, distanceLeft, distanceRight);
                        }
                        else if (send.Equals("LockTheDoors_Shoulder"))
                        {
                            ShoulderExercise2(wristDistance, angleLeft, angleRight);
                            gif1.Visibility = Visibility.Hidden;
                            gif3.Visibility = Visibility.Hidden;
                            gif2.Visibility = Visibility.Visible;
                            gif4.Visibility = Visibility.Hidden;
                            sheeda.Visibility = Visibility.Hidden;
                            loading.Visibility = Visibility.Hidden;
                        }
                        else if (send.Equals("headUpHeadDown_Neck"))
                        {
                            ShoulderExercise3(neckCenter);
                            gif3.Visibility = Visibility.Visible;
                            gif1.Visibility = Visibility.Hidden;
                            gif2.Visibility = Visibility.Hidden;
                            gif4.Visibility = Visibility.Hidden;
                            sheeda.Visibility = Visibility.Hidden;
                            loading.Visibility = Visibility.Hidden;
                        }
                        else if (send.Equals("earToShoulder_Neck"))
                        {
                            //Do Nothing..
                        }




                        // Copies skeleton data to an array of Skeletons, where each Skeleton contains a collection of the joints.
                        skeletonframe.CopySkeletonDataTo(skeletons);
                        //draw the Skeleton based on the Default Mode(Standing), "Seated"
                        if (sensor.SkeletonStream.TrackingMode == SkeletonTrackingMode.Default)
                        {
                            //Draw standing Skeleton
                            DrawStandingSkeletons(skeletons);
                        }
                        else if (sensor.SkeletonStream.TrackingMode == SkeletonTrackingMode.Seated)
                        {
                            //Draw a Seated Skeleton with 10 joints
                            DrawSeatedSkeletons(skeletons);
                        }
                    }
                }
            }
        }
        public int mcount = 0;
        public int sets = 0;
        public int seconds = 5;

        public bool flag = false;
        public bool flag2 = false;
        public void ShoulderExercise1(double angleLeft, double angleRight, double distanceLeft, double distanceRight) //overhead
        {
            if (flag == false)
            {
                msg.Content = "Please take your arms at 90 degrees angle.";
            }

            currentExercise = "OverHead_Shoulder";
            if (angleLeft > 75.0 && angleLeft < 105
                && angleRight > 75.0 && angleRight < 105.0
                && distanceLeft > 30.0
                && distanceRight > 30.0)
            {
                msg.Content = "Now move your arms straight up.";
                flag = true;
            }
            if (flag == true)
            {
                if (angleLeft > 140.0 && angleLeft < 160
                     && angleRight > 140.0 && angleRight < 160
                     && distanceLeft > 25.0 && distanceLeft < 45.0
                     && distanceRight > 25.0 && distanceRight < 45.0)

                {
                    trackedBonePen = new Pen(Brushes.Green, 6);
                    
                    mcount++;
                    count.Content = mcount + "";
                    flag = false;
                    flag2 = true;

                    if (mcount == 3)
                    {
                        TaDa.Play();
                        sets++;
                        setCount.Content = sets;
                        count.Content = 0;
                        getReadyIn.Content = "Get Ready In";
                        setCount.Content = " ";
                        SetNo.Content = "";
                        count.Content = "";
                        RepNo.Content = "";


                        if (sets == 1)
                        {
                            dispatcherTimer.Tick += dispatcherTimer_Tick;
                            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                            dispatcherTimer.Start();
                        }
                        else if (sets == 2)
                        {
                            dispatcherTimer2.Tick += dispatcherTimer_Tick;
                            dispatcherTimer2.Interval = new TimeSpan(0, 0, 1);
                            dispatcherTimer2.Start();
                        }
                        else if (sets == 3)
                        {
                            dispatcherTimer3.Tick += dispatcherTimer_Tick;
                            dispatcherTimer3.Interval = new TimeSpan(0, 0, 1);
                            dispatcherTimer3.Start();
                            setCount.Content = sets;
                            m_dbConnection.Open();
                            DateTime dt = DateTime.Now;
                            string sql1 = "insert into exerciseprogress (month,exercise) values (" + "'" + dt.Month + "'," + "'Overhead')";
                            SQLiteCommand command0 = new SQLiteCommand(sql1, m_dbConnection);
                            command0.ExecuteNonQuery();
                           
                        }
                        else
                        {
                            return;
                        }

                        RepNo.Content = "";
                        count.Content = "";
                        seconds = 15;

                    }
                    else
                    {
                        correct.Play();
                        getReadyIn.Content = "";
                    }

                }

            }
            if (flag2 == true)
            {
                if (angleLeft > 75.0 && angleLeft < 105
                && angleRight > 75.0 && angleRight < 105.0
                && distanceLeft > 30.0
                && distanceRight > 30.0)
                {
                    trackedBonePen = new Pen(Brushes.Red, 6);
                    flag = false;
                    flag2 = false;
                }

            }

        }
        public void ShoulderExercise2(double distanceWrist, double angleLeft, double angleRight) //lock the doors
        {
            
                msg.Content = "Stretch your arms at 180 degrees and rotate wrists";
            double[][] psroblem =
            {
               new double[] {angleLeft ,   angleRight, wristDistance , leftElbowShoulderShoulderCenter,
                   shoulderCenterShoulderLeftHead ,shoulderCenterShoulderRightHead ,rightElbowRightShoulderShoulderCenter ,
                   distanceLeft , distanceRight , leftShoulderElbowShoulderCenter  , rightShoulderElbowShoulderCenter},
            };

            // if (angleLeft > 130 && angleLeft < 185
            //     && angleRight > 130 && angleRight < 185
            //     && distanceWrist > 12 && distanceWrist < 28)
            Console.WriteLine(Program.func(psroblem));
                if (string.Equals(Program.func(psroblem).ToString(), "True"))
            {
                
                trackedBonePen = new Pen(Brushes.Green, 6);

                if (fzShoulder1 == false && prevSecFZ2 == false)
                {
                    currentExercise = "LockTheDoors_Shoulder";
                    FzShoulder1.Tick += dispatcherTimer_Tick;
                    FzShoulder1.Interval = new TimeSpan(0, 0, 1);
                    FzShoulder1.Start();
                }
                else if (fzShoulder1 == false && prevSecFZ2 == true)
                {
                    FzShoulder1.Start();
                    
                    if (m_dbConnection != null && m_dbConnection.State == ConnectionState.Closed)
                    {
                        m_dbConnection.Open();
                    }
                    DateTime dt = DateTime.Now;
                    string sql1 = "insert into exerciseprogress (month,exercise) values (" + "'" + dt.Month + "'," + "'LockTheDoors')";
                    SQLiteCommand command0 = new SQLiteCommand(sql1, m_dbConnection);
                    command0.ExecuteNonQuery();
                }
                fzShoulder1 = true;
                prevSecFZ2 = true;
            }
            else
            {
                trackedBonePen = new Pen(Brushes.Red, 6);
                FzShoulder1.Stop();
                fzShoulder1 = false;
            }

        }
        Boolean nextExerciseNeck = false;
        Boolean firstExercise = true;
        public void ShoulderExercise3(double neckCenter)
        {
            
            if (nextExerciseNeck == false && firstExercise == true)
            {
                msg.Content = "Move your head down. Look downwards.";
                if (neckCenter > 45 && neckCenter < 65)
                {
                    //timer1 here

                    trackedBonePen = new Pen(Brushes.Yellow, 6);
                    if (fzShoulder1 == false && prevSecFZ2 == false)
                    {
                        currentExercise = "headUpHeadDown_Neck";
                        FzShoulder1.Tick += dispatcherTimer_Tick;
                        FzShoulder1.Interval = new TimeSpan(0, 0, 1);
                        FzShoulder1.Start();
                    }
                    else if (fzShoulder1 == false && prevSecFZ2 == true)
                    {
                        FzShoulder1.Start();
                    }
                    fzShoulder1 = true;
                    prevSecFZ2 = true;

                    if (seconds == 0)
                    {
                        firstExercise = false;

                        msg.Content = "Move your head up, Look upwards";

                        nextExerciseNeck = true;
                    }
                }
                else
                {
                    trackedBonePen = new Pen(Brushes.Red, 6);
                    FzShoulder1.Stop();
                    fzShoulder1 = false;
                }
            }

            else
            {
                if ((neckCenter > 95 && neckCenter < 135) && nextExerciseNeck)
                {
                    trackedBonePen = new Pen(Brushes.Green, 6);
                    //timer 2 here

                    if (fzShoulder2 == false && prevSecFZ3 == false)
                    {
                        currentExercise = "headUpHeadDown_Neck";
                        FzShoulder2.Tick += dispatcherTimer_Tick;
                        FzShoulder2.Interval = new TimeSpan(0, 0, 1);
                        FzShoulder2.Start();
                        m_dbConnection.Open();
                        msg.Content = "Correct Stance";
                        DateTime dt = DateTime.Now;
                        string sql1 = "insert into exerciseprogress (month,exercise) values (" + "'" + dt.Month + "'," + "'ChinUpDown')";
                        SQLiteCommand command0 = new SQLiteCommand(sql1, m_dbConnection);
                        command0.ExecuteNonQuery();

                    }
                    else if (fzShoulder1 == false && prevSecFZ3 == true)
                    {
                        FzShoulder2.Start();
                    }
                    fzShoulder2 = true;
                    prevSecFZ3 = true;

                    if (seconds <= 0)
                    {
                        nextExerciseNeck = false;
                        firstExercise = true;
                    }
                }
                else
                {
                    trackedBonePen = new Pen(Brushes.Red, 6);
                    FzShoulder2.Stop();
                    fzShoulder1 = false;
                }

            }

        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (currentExercise.Equals("OverHead_Shoulder"))
            {
                if (seconds > 0)
                {
                    timer.Content = seconds--;
                }
                if (seconds == 0)
                {
                    RepNo.Content = "Rep No.";
                    count.Content = 0;
                    getReadyIn.Content = "";
                    timer.Content = "";
                    mcount = 0;
                    dispatcherTimer.Stop();
                    dispatcherTimer2.Stop();
                    dispatcherTimer3.Stop();

                    return;
                }
            }
            else if (currentExercise.Equals("headUpHeadDown_Neck"))
            {
                if (seconds >= 0)
                {
                    timer.Content = seconds--;
                    tick.Play();
                }
                if (seconds < 0)
                {
                    FzShoulder1.Stop();
                    FzShoulder2.Stop();
                    seconds = 5;
                    

                    return;
                }
            }
            else if (currentExercise.Equals("LockTheDoors_Shoulder"))
            {
                if (seconds >= 0)
                {
                    timer.Content = seconds--;
                    tick.Play();
                }
                if (seconds < 0)
                {
                    FzShoulder1.Stop();
                    correct.Play();
                    return;
                }
            }

        }
        //Thi Function Draws the Standing  or Default Skeleton
        private void DrawStandingSkeletons(Skeleton[] skeletons)
        {

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                //Draw a Transparent background to set the render size or our Canvas
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                //If the skeleton Array has items 
                if (skeletons.Length != 0)
                {
                    //Loop through the Skeleton joins
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);


                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(this.centerPointBrush,
                                           null,
                                           this.SkeletonPointToScreen(skel.Position), BodyCenterThickness, BodyCenterThickness);

                        }

                    }


                }

                //Prevent Drawing outside the canvas 
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));

            }
        }


        private void DrawSeatedSkeletons(Skeleton[] skeletons)
        {

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                //Draw a Transparent background to set the render size 
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);


                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(this.centerPointBrush,
                                           null,
                                           this.SkeletonPointToScreen(skel.Position), BodyCenterThickness, BodyCenterThickness);

                        }

                    }


                }

                //Prevent Drawing outside the canvas 
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));

            }
        }

        // Draws indicators to show which edges are clipping skeleton data
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            // if the skeleton goes out of frame at the bottom
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }
            // if the skeleton goes out of frame at the top
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }
            // if the skeleton goes out of frame at the left
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }
            // if the skeleton goes out of frame at the right
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }

        // Draws a skeleton's bones and joints
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        // Draws a bone line between two joints
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked || joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred && joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;

            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
        }

        // Maps a SkeletonPoint to lie within our render space and converts to Point
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        private void report_Click(object sender, RoutedEventArgs e)
        {
            if (send.Equals("OverHead_Shoulder"))
            {
                String textMsg = "Angle b/w left <wrist, elbow, shoulder> (degree): " + angleLeft +
                    "\nAngle b/w right <wrist, elbow, shoulder> (degree): " + angleRight +
                    "\nDistance between left <wrist, spine> (m): " + distanceLeft +
                    "\nDistance between right <wrist, spine> (m): " + distanceRight;
                var msg = new CustomMaterialMessageBox
                {

                    TxtMessage = { Text = textMsg },
                    TxtTitle = { Text = "Report For OverHead (Shoulder)", Foreground = Brushes.White },

                    BtnOk = { Content = "Save" },
                    BtnCancel = { Content = "Cancel" },
                    //MainContentControl = { Background = Brushes.MediumVioletRed },
                    //TitleBackgroundPanel = { Background = Brushes.BlueViolet },

                    //BorderBrush = Brushes.BlueViolet
                };
                msg.Show();
                var results = msg.Result;
                if (results + "" == "OK")
                {
                    saveResult(textMsg);
                }
            }



            else if (send.Equals("LockTheDoors_Shoulder"))
            {
                String textMsg = "Angle b/w left <wrist, elbow, shoulder> (degree): " + angleLeft +
                     "\nAngle b/w right <wrist, elbow, shoulder> (degree): " + angleRight +
                     "\nDistance between  <left wrist, right wrist> (m): " + wristDistance;

                var msg = new CustomMaterialMessageBox
                {


                    TxtMessage = { Text = textMsg },
                    TxtTitle = { Text = "Report For LockTheDoors (Shoulder)", Foreground = Brushes.White },

                    BtnOk = { Content = "Save" },
                    BtnCancel = { Content = "Cancel" },
                };
                msg.Show();
                var results = msg.Result;
                if (results + "" == "OK")
                {
                    saveResult(textMsg);
                }
            }
            else if (send.Equals("headUpHeadDown_Neck"))
            {
                String textMsg = "Angle b/w  <neck, shoulder center, spine> (degree): " + neckCenter;
                var msg = new CustomMaterialMessageBox
                {

                    TxtMessage = { Text = textMsg },
                    TxtTitle = { Text = "Report For headUpHeadDown (Neck)", Foreground = Brushes.White },

                    BtnOk = { Content = "Save" },
                    BtnCancel = { Content = "Cancel" },
                };
                msg.Show();
                var results = msg.Result;

                if (results + "" == "OK")
                {
                    saveResult(textMsg);
                }
            }
            else if (send.Equals("earToShoulder_Neck"))
            {
                String textMsg = "Angle b/w  <neck, shoulder center, spine> (degree): " + neckCenter;
                var msg = new CustomMaterialMessageBox
                {

                    TxtMessage = { Text = textMsg },
                    TxtTitle = { Text = "Report For earToShoulder (Neck)", Foreground = Brushes.White },


                    BtnOk = { Content = "Save" },
                    BtnCancel = { Content = "Cancel" },
                };
                msg.Show();
                var results = msg.Result;

                if (results + "" == "OK")
                {
                    saveResult(textMsg);
                }
            }
        }
        Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomCenter,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });
        private void saveResult(String textMsg)
        {
            string fileText = textMsg;

            Win32.SaveFileDialog dialog = new Win32.SaveFileDialog()
            {
                Filter = "Text Files(*.txt)|*.txt|All(*.*)|*"
            };

            if (dialog.ShowDialog() == true)
            {
                File.WriteAllText(dialog.FileName, fileText);
                notifier.ShowSuccess("File saved successfuly.");
            }
        }
    }
}


