﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    public class currentMode
    {
        public String crMode;
        public currentMode(){
            this.crMode = null;
        }

        public void setMode(string mode)
        {
            this.crMode = mode;
        }

        public String getMode()
        {
            return this.crMode;
        }
    }
}
