﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for SplashScreen3.xaml
    /// </summary>
    public partial class SplashScreen3 : Window
    {
        public SplashScreen3()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SplashScreen2 splash = new SplashScreen2();
            splash.Show();
            this.Hide();
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            SplashScreen splash = new SplashScreen();
            splash.Show();
            this.Hide();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ExerciseList exer = new ExerciseList();
            exer.Show();
            this.Hide();
        }
    }
}
