﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Window
    {

        Notifier notifier = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomCenter,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public Signup()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string query = "insert into auth (username, password) values (" + "'" + username.Text + "'" + ",  " + "'" + password.Text + "'" + ")";
            SQLiteCommand command = new SQLiteCommand(query, ExerciseList.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            command = new SQLiteCommand(query, ExerciseList.m_dbConnection);
            command.ExecuteNonQuery();

            notifier.ShowSuccess("User created successfully");
            /*string sql = "select * from auth";
            SQLiteCommand commands = new SQLiteCommand(sql, ExerciseList.m_dbConnection);
            SQLiteDataReader readers = commands.ExecuteReader();
            while (readers.Read())
            Console.WriteLine("Name: " + readers["username"] + "\tScore: " + readers["password"]);
            //command.ExecuteNonQuery();*/
        }
    }
}
